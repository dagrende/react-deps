const parser = require("@babel/parser");
const JSPath = require('jspath');

function deps2dot(prog, filePath, ws) {
  console.log("prog length: %d", prog.length);
  const ast = parser.parse(prog, {
    // parse in strict mode and allow module declarations
    sourceType: "module",

    plugins: [
      // enable jsx syntax
      "jsx",
    ],
  });

  ws.write("digraph G {"+ '\n');
  ws.write(' rankdir="LR";' + '\n');
  let id = 0;

  // get all useEffect calls at top level of export default function
  const useEffectCalls = JSPath.apply(
    `.program.body{.type == "ExportDefaultDeclaration"}
     .declaration.body.body{.type === "ExpressionStatement"
       && .expression{.type == "CallExpression" && .callee.name == "useEffect"}}`
      , ast);

  // collect details about the useEffect calls, as dependents
  const dependents = useEffectCalls.map((ue) => ({
    name: getName(ue),
    file: filePath,
    line: ue.loc.start.line,
    dependees: getDependees(ue),
  }));

  // get name of ue from comment above the call, or as autoi where is a number
  function getName(ue) {
    const lc = ue.leadingComments;
    return (lc && lc[lc.length - 1].value.trim()) || "auto" + id++;
  }

  // get second useEffect argument items - an array of variables, usually
  function getDependees(ue) {
    return JSPath.apply(`.expression.arguments[1].elements`, ue).map((item) => prog.substring(item.start, item.end));
  }

  const dependeeSet = new Set();

  // generate a node for each dependent with name and tooltip
  // and an arrow to each dependee
  for (const dep of dependents) {
    const nid = `l${id++}`;
    const depArg = (dep.dependees && dep.dependees.length === 0 && " []") || "";
    ws.write(` ${nid} [label="${dep.name}${depArg}" tooltip="${dep.file}:${dep.line}"];\n`);
    if (dep.dependees) {
      for (const dependee of dep.dependees) {
        dependeeSet.add(dependee);
        ws.write(`  "${nid}" -> "${dependee}" [tooltip="depends on"];` + '\n');
      }
    }
  }
  ws.write('\n');

  // generate a shape (no border) for each unique dependee
  for (const dependee of dependeeSet) {
    ws.write(` "${dependee}" [shape=none];` + '\n');
  }
  ws.write("}" + '\n');
}

module.exports = {
	deps2dot
}